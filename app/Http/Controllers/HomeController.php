<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todolist;

class HomeController extends Controller
{

    public $successStatus = 200;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetchToDoList()
    {
        $todolist  = Todolist::get();

        if($todolist)
        {
            return response()->json(['success' => $todolist, 'message' => 'list fetched successfully'], $this->successStatus);
        }
        else
        {
            return response()->json(['error'=> 'List is empty'], 401);
        }
    }

    public function addToDoList(Request $request)
    {
        if ($request->input('content'))
        {
            $content = $request->input('content');
            $todolist  = new Todolist;
            $todolist->todo_list_items = $content;
            $todolist->save();

            if($todolist)
            {
                return response()->json(['success' => $todolist, 'message' => 'Item added successfully'], $this->successStatus);
            }
            else
            {
                return response()->json(['error'=> 'Insertion failed'], 401);
            }
        }
        else
        {
                return response()->json(['error'=> 'No content found, please check inputs'], 401);
        }
    }



    public function updateToDoList(Request $request)
    {
        if ($request->input('content') && $request->input('id'))
        {
            $content = $request->input('content');
            $idOfToDoListItem = $request->input('id');
            if (Todolist::where('id', $idOfToDoListItem)->first())
            {
                $todolist  = Todolist::where('id', $idOfToDoListItem)->first();
                $todolist->todo_list_items = $content;
                $todolist->save();
                if($todolist)
                {
                    return response()->json(['success' => $todolist, 'message' => 'Item updated successfully'], $this->successStatus);
                }
                else
                {
                    return response()->json(['error'=> 'Insertion failed'], 401);
                }
            }
            else
            {
                return response()->json(['error'=> 'No content found, please check inputs'], 401);
            }
        }
        else
        {
            return response()->json(['error'=> 'No content found, please check inputs'], 401);
        }
    }

    public function removeItemFromToDoList($idOfToDoListItem)
    {
        if (Todolist::where('id', $idOfToDoListItem)->first())
        {
            $todolist = Todolist::where('id', $idOfToDoListItem)->first();
            $deleteStatus  = $todolist->delete();
            return response()->json(['success' => $deleteStatus, 'message' => 'Item removed successfully'], $this->successStatus);
        }
        else
        {
            return response()->json(['error'=>'The targeted todo list item does not exist, please check inputs'], 401);
        }
    }
}
