<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('fetch-to-do-list', 'HomeController@fetchToDoList');
Route::post('add-to-do-list', 'HomeController@addToDoList');
Route::post('update-to-do-list', 'HomeController@updateToDoList');
Route::get('remove-to-do-list/{idOfToDoListItem}', 'HomeController@removeItemFromToDoList');